import unittest

from xmit_processor.xmit_processor import preprocess

class TestPreprocess(unittest.TestCase):
    def test_invalid_wav_file(self):
        """
        Basic test of preprocess handling invalid wav file
        """
        result = preprocess('/tmp/input_wav/test_file')
        self.assertEqual(None, result)

if __name__ == '__main__':
    unittest.main()
