Predict whether audio file is someone talking, data or something to be ignored.  Uses Tensorflow Model Server and pre-trained model.

to test
-------

python -m unittest test/basic.py
