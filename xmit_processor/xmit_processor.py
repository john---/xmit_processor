#!/usr/bin/env python3

import os
import yaml
import os.path
import logging
import tensorflow as tf
import json
import numpy as np
import os

import asyncio
from aiohttp import web


dir = os.path.abspath(os.path.dirname(__file__))
logging.basicConfig(filename='%s/xmit_processor.log'%(dir), level=logging.DEBUG, format='%(asctime)s %(message)s')

# load config
with open('%s/xmit_processor.conf'%(dir), 'r') as ymlfile:
    cfg = yaml.load(ymlfile, Loader=yaml.SafeLoader)

def decode_audio(audio_binary):
  audio, _ = tf.audio.decode_wav(audio_binary)
  return tf.squeeze(audio, axis=-1)

async def delete_xmit(file):
    await asyncio.sleep(3)
    os.remove(file)
    logging.debug(f'shot xmit delete complete {file}')

def get_audio_info(file):
  audio_binary = tf.io.read_file(file)
  waveform = decode_audio(audio_binary)

  total_samples = tf.size(waveform).numpy()
  sample_rate = cfg['audio']['sample_rate']
  length = round(total_samples/sample_rate, 3)

  return length, waveform, total_samples

# convert the waveform into a spectrogram
def get_spectrogram(file):
  length, waveform, total_samples = get_audio_info(file)

  sample_rate = cfg['audio']['sample_rate']
  clip_length = cfg['audio']['clip_length']
  target_samples = sample_rate * clip_length

  # Shorten to target length
  # TODO:  add this to preprocess for training and get rid of pre-shortening
  #        when creating training data
  logging.debug('current elements: %d', tf.size(waveform).numpy())
  logging.debug('number of axes: %d', waveform.ndim)
  middle = total_samples/2
  start = int(middle - target_samples/2)
  if start < 0:
      start = 0
  end = int(middle + target_samples/2)
  waveform = waveform[start:end]
  logging.debug('length: %d [ start: %d middle: %d end: %d]', tf.size(waveform).numpy(), start, middle, end)

  # Padding for files with less than 16000 samples (2 seconds of 8 Khz sample rate)
  zero_padding = tf.zeros([target_samples] - tf.shape(waveform), dtype=tf.float32)

  # Concatenate audio with padding so that all audio clips will be of the
  # same length
  waveform = tf.cast(waveform, tf.float32)
  equal_length = tf.concat([waveform, zero_padding], 0)
  spectrogram = tf.signal.stft(
      equal_length, frame_length=255, frame_step=128)

  spectrogram = tf.abs(spectrogram)

  spectro = []
  spectro.append(spectrogram.numpy())
  spectro = np.expand_dims(spectro, axis=-1) # TODO:  what is this dimenision for?

  return spectro, length

def predict(spectrogram):
    if spectrogram is None:
        return None, None

    # prediction = model(spectrogram) # full TF

    #input_shape = input_details[0]['shape']
    input_data = spectrogram
    model.set_tensor(input_details[0]['index'], input_data)

    model.invoke()

    prediction = model.get_tensor(output_details[0]['index'])
    
    types = ['V', 'D', 'S']
    logging.debug('the model predicted this was a: %s', types[np.argmax(prediction[0])])

    predictions = {}
    for index, a_pred in np.ndenumerate(prediction[0]):
        predictions[types[index[0]]] = round(float(a_pred), 3)

    return types[np.argmax(prediction[0])], predictions

def predict_via_server(spectrogram):
    if spectrogram is None:
        return None

    data = json.dumps({"signature_name": "serving_default", "instances": spectrogram.tolist()})

    import requests
    headers = {"content-type": "application/json"}
    json_response = requests.post('http://localhost:8501/v1/models/xmit_model:predict', data=data, headers=headers)
    prediction = json.loads(json_response.text)['predictions']

    types = ['voice', 'data', 'skip']
    logging.info('the model predicted this was a: %s', types[np.argmax(prediction[0])])

    return prediction

def load_model():
    #model = tf.keras.models.load_model(cfg['model']['location'])  #  full TF
    model = tf.lite.Interpreter(model_path=cfg['model']['location'])
    model.allocate_tensors()
    input_details = model.get_input_details()
    output_details = model.get_output_details()
    logging.info('model loaded')
    return model, input_details, output_details

def init_db():
    import psycopg2 # for postgres support
    import psycopg2.extensions
    import psycopg2.extras

    try:
        conn = psycopg2.connect(cfg['publish']['db'])
        conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
        curs = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
        curs.execute('select version()')
        version = curs.fetchone()['version']
        logging.info('publication to postgresql enabled: %s', version)
    except Exception as e:
        curs = None
        logging.error(e)
        logging.error('publication to postgresql disabled')

    return curs

def embellish(msg):
    import re
    fr = re.compile('(.*?)_.*_([VDS]).*\.wav')
    res = fr.match(msg['file'])
    msg['freq'] = res.group(1)
    msg['detected_as'] = res.group(2)

    curs.execute('select freq_key, freq, label, bank, pass from freqs where freq = %s and bank = any(%s) limit 1', (msg['freq'], cfg['banks']))  # check for multiple banks
                               # TODO: under what situations can there be more than one across scanned banks? 
    entry = curs.fetchone()

    if entry == None:  # if no entry then create one
        logging.debug('no entry for this freq and bank')
        bank = cfg['banks'][0]
        label = 'Unknown'
        curs.execute('insert into freqs (freq, label, bank, source) values (%s, %s, %s, %s) returning freq_key', (msg['freq'], 'Unknown', bank, 'search'))
        freq_key = curs.fetchone()['freq_key']

        entry = {
            'label': label,
            'bank': bank,
            'freq_key': freq_key,
            'pass': 0
        }

    msg.update(entry)

    if msg['detected_as'] == 'V':
        voice_detected = True
        logging.debug('detected voice')
    else:
        voice_detected = False
        logging.debug('detected data/skip')

    curs.execute('insert into xmit_history (freq_key, source, file, duration, detect_voice) values (%s, %s, %s, \'%s\', %s) returning xmit_key', (msg['freq_key'], 'dongle1', msg['file'], msg['duration'], voice_detected))
    msg['xmit_key'] = curs.fetchone()['xmit_key']

    msg['class'] = 'U'  # this is the column default so pass it on

    return msg

def publish(msg):
    if not bool(msg) or curs == None or msg['detected_as'] == None:
        return
    logging.info('to be published: %s', msg)
    msg_json = json.dumps(msg)
    curs.execute("NOTIFY %s, '%s';" % (cfg['publish']['channel'], msg_json))

def file_exists(file):
    if os.path.isfile(file):
        logging.info('audio: %s', file)
        return {
            'type': 'audio'
             }

    raise FileNotFoundError(f'File not found: {file}')

def classify(file):
    msg = file_exists(file)
    spectrogram, msg['duration'] = get_spectrogram(file)
    msg['detected_as'], msg['prediction'] = predict(spectrogram)

    return msg

async def watch_and_publish():
    from minotaur import Inotify, Mask
    with Inotify(blocking=False) as n:
        n.add_watch(cfg['audio']['src'], Mask.MOVED_TO)
        async for evt in n:
            try:
                file = '{}/{}'.format(cfg['audio']['src'], str(evt.name))
                msg = file_exists(file)
                msg['source'] = 'scanner'
                msg['file'] = str(evt.name)
                msg['duration'] = get_audio_info(file)[0]
                msg = embellish(msg)
                publish(msg)
            except Exception as error:
                logging.debug(f'unknown problem processing xmit: {str(evt.name)} {error}')
              
async def classify_only_handler(request):
    req = await request.json()
    logging.info('req: %s', req)
    for cmd in req:
        if cmd == 'classify':
            for root, dir, files in os.walk(cfg['audio']['src']):
                if req['classify'] in files:  # find first occurance of sound file
                    file = '{}/{}'.format(root, req['classify'])
                    msg = classify(file)
                    msg['file'] = req['classify']
                    resp = {'classification': msg['prediction']}
                    break
                else:
                    resp = {'error': 'File not found: ' + req['classify']}
        else:
             resp = {'error': 'invalid command'}
    return web.json_response(resp)

model, input_details, output_details = load_model()

async def main():
    global curs

    curs = init_db()

    app = web.Application()
    app.add_routes([web.get('/', classify_only_handler)])

    loop = asyncio.get_event_loop()
    loop.create_task(watch_and_publish())

    runner = web.AppRunner(app)
    await runner.setup()
    await web.TCPSite(runner, '0.0.0.0', '8080').start()
    await asyncio.get_running_loop().create_future()

asyncio.run(main())

